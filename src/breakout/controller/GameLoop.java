package breakout.controller;

/**
 * Defines the behavior of a gameloop.
 */
public interface GameLoop {

    /**
     * Starts the loop.
     */
    void start();

    /**
     * Resumes the loop.
     */
    void unPause();

    /**
     * Stops the loop.
     */
    void pause();

    /**
     * @return true if the loop is running; false otherwise.
     */
    boolean isPaused();

}
