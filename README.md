## BREAKOUT ##
Lo scopo del gioco è quello di abbattere un muro di mattoni sfruttando il rimbalzo di una pallina su di una barra posta nella parte inferiore dello schermo. Il compito del giocatore, comandando la barra , è quello di eliminare tutti i blocchi presenti senza far cadere la pallina sul fondo.
Features:

* Partita in modalità classica (ispirata alla versione originale del 1976)
*  Partita in modalità avanzata (con elementi di gioco aggiuntivi, più simili alle versioni recenti) 
*  Possibilità di salvare i propri punteggi ed inserirli in una classifica locale
*  Editor per la creazione di livelli personalizzati

Sviluppato da:
Riccardo Salvatori, Stefano Salvatori e Mattia Minelli.